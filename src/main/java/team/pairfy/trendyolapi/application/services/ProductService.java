package team.pairfy.trendyolapi.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.pairfy.trendyolapi.domain.Category;
import team.pairfy.trendyolapi.domain.Product;
import team.pairfy.trendyolapi.infra.api.mapper.ProductMapper;
import team.pairfy.trendyolapi.infra.api.model.request.product.CreateProductRequest;
import team.pairfy.trendyolapi.infra.api.model.request.product.DeleteProductRequest;
import team.pairfy.trendyolapi.infra.api.model.request.product.UpdateProductRequest;
import team.pairfy.trendyolapi.infra.api.model.response.CategoryResponse;
import team.pairfy.trendyolapi.infra.api.model.response.ProductResponse;
import team.pairfy.trendyolapi.infra.repository.ProductRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryService categoryService;

    public List<ProductResponse> getAllProduct() {
        Iterable<Product> productRepositoryAll = productRepository.findAll();
        Iterator<Product> productIterator = productRepositoryAll.iterator();

        List<Product> productList = new ArrayList<>();

        while (productIterator.hasNext())
            productList.add(productIterator.next());

        return productList
                .stream()
                .map(product -> new ProductResponse(product.getName(), product.getPrice(), product.getUnit()))
                .collect(Collectors.toList());
    }

    public List<ProductResponse> getByName(String name) {
        List<Product> productList = productRepository.findByName(name);

        return productList
                .stream()
                .map(product -> new ProductResponse(product.getName(), product.getPrice(), product.getUnit()))
                .collect(Collectors.toList());
    }

    public Product create(CreateProductRequest createProductRequest) {
        Product product = ProductMapper.map(createProductRequest);

        // CategoryResponse response = categoryService.getCategoryById(createProductRequest.getCategory_id());
        product.setCategory(new Category(createProductRequest.getCategory_id()));

        return productRepository.save(product);
    }

    public Product update(UpdateProductRequest updateProductRequest) {
        Product product = ProductMapper.map(updateProductRequest);
        return productRepository.save(product);
    }

    public Object delete(DeleteProductRequest deleteProductRequest) {
        Optional<Product> productOptional = productRepository.findById(deleteProductRequest.getId());

        if (!productOptional.isPresent()) return "silmek istediginiz ıd'ye ait product yoktur";

        Product product = productOptional.get();

        productRepository.delete(product);

        ProductResponse productResponse = ProductMapper.map(product);

        return productResponse;
    }
}
/*
Optional<Category> categoryOptional = categoryRepository.findById(id);

        if (!categoryOptional.isPresent()) return null;

        Category category = categoryOptional.get();

        CategoryResponse categoryResponse = CategoryMapper.map(category);

        return categoryResponse;
 */