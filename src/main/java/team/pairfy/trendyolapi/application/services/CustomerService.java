package team.pairfy.trendyolapi.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.pairfy.trendyolapi.domain.Customer;
import team.pairfy.trendyolapi.infra.api.mapper.CustomerMapper;
import team.pairfy.trendyolapi.infra.api.model.request.customer.CreateCustomer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.DeleteCustomer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.UpdateCustomer;
import team.pairfy.trendyolapi.infra.api.model.response.CustomerResponse;
import team.pairfy.trendyolapi.infra.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public List<CustomerResponse> getAllCustomer() {
        Iterator<Customer> customerIterator = customerRepository.findAll().iterator();
        List<Customer> customerList = new ArrayList<>();

        while (customerIterator.hasNext())
            customerList.add(customerIterator.next());

        return customerList
                .stream()
                .map(customer -> new CustomerResponse(customer.getName(), customer.getSurname(), customer.getCity(), customer.getEmail()))
                .collect(Collectors.toList());
    }

    public String createCustomer(CreateCustomer createCustomer) {
        Customer customer = CustomerMapper.map(createCustomer);
        Customer save = customerRepository.save(customer);
        return "ok";
    }

    public Customer updateCustomer(UpdateCustomer updateCustomer) {
        Customer customer = CustomerMapper.map(updateCustomer);
        return customerRepository.save(customer);
    }

    public void deleteCustomer(DeleteCustomer deleteCustomer) {
        Customer customer = CustomerMapper.map(deleteCustomer);

        customerRepository.deleteById(customer.getId());
    }
}
