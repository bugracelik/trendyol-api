package team.pairfy.trendyolapi.application.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.pairfy.trendyolapi.domain.User;
import team.pairfy.trendyolapi.infra.repository.UserRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAllUsers() {
        Iterator<User> iterator = userRepository.findAll().iterator();
        List<User> userList = new ArrayList();

        while (iterator.hasNext())
            userList.add(iterator.next());

        return userList;
    }
}
