package team.pairfy.trendyolapi.infra.api.mapper;

import team.pairfy.trendyolapi.domain.Supplier;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.CreateSupplierRequest;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.UpdateSupplierRequest;

public class SupplierMapper {
    public static Supplier map(CreateSupplierRequest createSupplierRequest) {
        Supplier supplier = new Supplier();

        supplier.setAddress(createSupplierRequest.getAdress());
        supplier.setCity(createSupplierRequest.getCity());
        supplier.setCountry(createSupplierRequest.getCountry());
        supplier.setName(createSupplierRequest.getName());

        return supplier;
    }

    public static Supplier map(UpdateSupplierRequest updateSupplierRequest) {
        Supplier supplier = new Supplier();

        supplier.setId(updateSupplierRequest.getId());
        supplier.setAddress(updateSupplierRequest.getAddress());
        supplier.setCity(updateSupplierRequest.getCity());
        supplier.setCountry(updateSupplierRequest.getCountry());
        supplier.setName(updateSupplierRequest.getName());

        return supplier;
    }
}
