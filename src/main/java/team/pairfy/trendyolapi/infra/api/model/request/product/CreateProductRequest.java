package team.pairfy.trendyolapi.infra.api.model.request.product;

public class CreateProductRequest {
    private String name;
    private double price;
    private Integer supplier_id;
    private String unit;
    private Integer category_id;

    public CreateProductRequest() {
    }

    public CreateProductRequest(String name, double price, Integer supplier_id, String unit, Integer category_id) {
        this.name = name;
        this.price = price;
        this.supplier_id = supplier_id;
        this.unit = unit;
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(Integer supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }
}
