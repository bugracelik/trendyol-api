package team.pairfy.trendyolapi.infra.api.mapper;

import team.pairfy.trendyolapi.domain.Category;
import team.pairfy.trendyolapi.infra.api.model.response.CategoryResponse;

public class CategoryMapper {

    public static CategoryResponse map(Category category) {
        return new CategoryResponse(category.getId(), category.getName());
    }
}
