package team.pairfy.trendyolapi.infra.api.model.request.product;

public class DeleteProductRequest {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
