package team.pairfy.trendyolapi.infra.api.model.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import team.pairfy.trendyolapi.infra.reset.model.response.GetAllUserResponse;

import java.util.List;

public class UserResponse {
    public static ResponseEntity response(List<GetAllUserResponse> getAllUserResponses) throws JsonProcessingException {
        String stringAsJson = new ObjectMapper().writeValueAsString(getAllUserResponses);
        return new ResponseEntity(stringAsJson, HttpStatus.OK);
    }
}
