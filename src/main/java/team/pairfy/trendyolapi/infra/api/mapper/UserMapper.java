package team.pairfy.trendyolapi.infra.api.mapper;

import team.pairfy.trendyolapi.domain.User;
import team.pairfy.trendyolapi.infra.reset.model.response.GetAllUserResponse;

import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {
    public static List<GetAllUserResponse> userMap(List<User> allUsers) {
        List<GetAllUserResponse> collect = allUsers
                .stream()
                .map(user -> new GetAllUserResponse(user.getEmail(), user.getUserId()))
                .collect(Collectors.toList());

        return collect;
    }
}
