package team.pairfy.trendyolapi.infra.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.pairfy.trendyolapi.application.services.SupplierService;
import team.pairfy.trendyolapi.domain.Supplier;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.CreateSupplierRequest;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.DeleteSupplierRequest;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.UpdateSupplierRequest;
import team.pairfy.trendyolapi.infra.api.model.response.SupplierResponse;
import team.pairfy.trendyolapi.infra.repository.SupplierRepository;

import java.util.List;

@RestController
@RequestMapping("suppliers")
public class SupplierController { // SPRING BEAN

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    SupplierService supplierService;

    @GetMapping("/id/{id}")
    public SupplierResponse getCategoryById(@PathVariable Integer id) {
        return supplierService.getCategoryById(id);
    }


    @GetMapping("/city/{city}")
    public List<SupplierResponse> getSupplierByCity(@PathVariable String city) {
        return supplierService.getSupplierByCity(city);
    }

    @GetMapping("/city/{city}/name/{name}")
    public List<SupplierResponse> getSupplierByCityandName(@PathVariable String city, @PathVariable String name) {
        return supplierService.getSupplierByCityandName(city, name);
    }

    @GetMapping("/name/{name}")
    public List<SupplierResponse> getSupplierByName(@PathVariable String name) {
        return supplierService.getSupplierByName(name);
    }

    @GetMapping("/country/{country}")
    public List<SupplierResponse> getSupplierByCountry(@PathVariable String country) {
        return supplierService.getSupplierByCountry(country);
    }

    @PostMapping("create")
    public Supplier create(@RequestBody CreateSupplierRequest createSupplierRequest) {
        return supplierService.create(createSupplierRequest);
    }

    @PutMapping("update")
    public Supplier update(@RequestBody UpdateSupplierRequest updateSupplierRequest) {
        return supplierService.update(updateSupplierRequest);
    }

    @DeleteMapping("delete")
    public void deleteById(@RequestBody DeleteSupplierRequest deleteSupplierRequest) {
        supplierService.deleteById(deleteSupplierRequest);
    }

    @GetMapping("count/{name}")
    public String countByName(@PathVariable String name) {
        return supplierService.countByName(name);
    }

    @GetMapping("count")
    public String count() {
        return supplierService.count();
    }
}
