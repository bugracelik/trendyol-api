package team.pairfy.trendyolapi.infra.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import team.pairfy.trendyolapi.application.services.CategoryService;
import team.pairfy.trendyolapi.domain.Category;
import team.pairfy.trendyolapi.infra.api.model.request.category.DeleteCategoryRequest;
import team.pairfy.trendyolapi.infra.api.model.request.category.UpdateCategoryRequest;
import team.pairfy.trendyolapi.infra.repository.CategoryRepository;
import team.pairfy.trendyolapi.infra.api.model.request.category.CreateCategoryRequest;
import team.pairfy.trendyolapi.infra.api.model.response.CategoryResponse;

import java.util.List;

@RestController
@RequestMapping("category")
public class CategoryController { // SPRING BEAN

    public void category() {
        System.out.println("springimiz ayaga kalkmıştır hayırlı olsun :)");
    }

    @Autowired
    CategoryService categoryService;

    @Autowired
    CategoryRepository categoryRepository;

    @PostMapping("/create")
    public String createCategory(@RequestBody CreateCategoryRequest createCategoryRequest) {
        return categoryService.createCategory(createCategoryRequest);
    }

    @PostMapping("/createmany")
    public String createCategoryMany(@RequestBody List<CreateCategoryRequest> createCategoryRequestList) {
        return categoryService.createCategoryMany(createCategoryRequestList);
    }

    @GetMapping("/id/{id}")
    public CategoryResponse getCategoryById(@PathVariable Integer id) {
        return categoryService.getCategoryById(id);
    }

    @GetMapping("/name/{name}")
    public List<CategoryResponse> getCategoryByName(@PathVariable String name) {
        return categoryService.getCategoryByName(name);
    }

    @GetMapping("/id/{id}/name/{name}")
    public List<CategoryResponse> getCategoryByIdAndName(@PathVariable Integer id, @PathVariable String name) {
        return categoryService.getCategoryByIdAndName(id, name);
    }

    @GetMapping("/getAllCategory")
    public Iterable<Category> getAllCategory() {
        return categoryRepository.findAll();
    }

    @GetMapping("/getAllCategory/withResponse")
    public List<CategoryResponse> getAllCategoryWithResponse() {
        return categoryService.getAllCategoryWithResponse();
    }

    @GetMapping("/existsById/{id}")
    public ResponseEntity existsCategoryById(@PathVariable Integer id) {
        return categoryService.existsCategoryById(id);
    }

    @PutMapping("/updateCategory")
    public void updateCategory(@RequestBody UpdateCategoryRequest categoryUpdateRequest) {
        categoryService.updateCategory(categoryUpdateRequest);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        categoryService.delete(id);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestBody DeleteCategoryRequest deleteCategoryRequest) {
        categoryService.delete(deleteCategoryRequest);
    }

    @GetMapping("/description/{description}")
    public List<Category> findByDescription(@PathVariable String description) {
       return categoryService.findByDescription(description);
    }

    @GetMapping("/count")
    public String count() {
        return categoryService.count();
    }

}


