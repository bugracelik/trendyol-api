package team.pairfy.trendyolapi.infra.api.model.request.supplier;

public class UpdateSupplierRequest {
    private Integer id;
    private String address;
    private String city;
    private String country;
    private String name;

    public UpdateSupplierRequest() {
    }

    public UpdateSupplierRequest(Integer id, String address, String city, String country, String name) {
        this.id = id;
        this.address = address;
        this.city = city;
        this.country = country;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
