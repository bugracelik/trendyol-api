package team.pairfy.trendyolapi.infra.reset.model.response;

public class GetAllUserResponse {
    private  String userId;
    private  Object email;

    public GetAllUserResponse(String email, String userId) {
        this.email = email;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }
}
