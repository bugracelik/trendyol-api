package team.pairfy.trendyolapi.infra.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BugraConfig {

    @Bean // sadece çağrı
    public void config() {
        SampleConfig sample = new SampleConfig();

        sample.config();
    }

    @Bean
    public SampleConfig foo() {
        return new SampleConfig();
    }

    public void bar() {

    }

    @Bean
    public void tar () {
        System.out.println("spirng ayaga kaldırılıyor...");
    }

}
