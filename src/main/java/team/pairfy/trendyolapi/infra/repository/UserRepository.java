package team.pairfy.trendyolapi.infra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.trendyolapi.domain.User;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> { // CategoryRepositoryImpl
    Optional<User> findByUserId(String userId);
}